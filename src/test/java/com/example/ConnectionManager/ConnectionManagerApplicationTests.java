package com.example.ConnectionManager;

import com.example.ConnectionManager.dbs.ConnectionManager;
import com.example.ConnectionManager.dbs.ConnectionPool;
import com.example.ConnectionManager.properties.DatabaseProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class ConnectionManagerApplicationTests {

	@Autowired
	DatabaseProperties databaseProperties;

	@Resource
	public void setDatabaseProperties(DatabaseProperties databaseProperties) {
		this.databaseProperties = databaseProperties;
	}

	@Test
	public void whenCalledGetConnection_thenCorrect() throws SQLException {
		ConnectionPool connectionPool = ConnectionManager.create(
				databaseProperties.getUrlMaster(),
				databaseProperties.getUserMaster(),
				databaseProperties.getPasswordMaster());
		assertTrue(connectionPool.getConnection().isValid(1));
		assertEquals(connectionPool.getUser(),"master");
		assertEquals(connectionPool.getSize(), 10);
	}

	@Test
	public void whenCalledGetConnectionSlave_thenCorrect() throws SQLException {
		ConnectionPool connectionPool = ConnectionManager.create(
				databaseProperties.getUrlSlave(),
				databaseProperties.getUserSlave(),
				databaseProperties.getPasswordSlave());
		assertTrue(connectionPool.getConnection().isValid(1));
		assertTrue(connectionPool.getConnection().isValid(1));
		assertEquals(connectionPool.getUsedConnections(),2);
		assertEquals(connectionPool.getUser(),"slave");
	}
}
