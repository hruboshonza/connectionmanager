package com.example.ConnectionManager.dbs;

import com.example.ConnectionManager.properties.DatabaseProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.annotation.Resource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class ConnectionManager implements ConnectionPool {

    private String url;
    private String user;
    private String password;
    private List<Connection> connectionPool;
    private List<Connection> usedConnections = new ArrayList<>();
    private static int INITIAL_POOL_SIZE = 10;
    private static int MAX_POOL_SIZE = 20;
    private static int MAX_TIMEOUT = 1;

    private static DatabaseProperties databaseProperties;

    public ConnectionManager() {
    }

    public ConnectionManager(String url, String user, String password, List<Connection> connectionPool) {
        this.url = url;
        this.user = user;
        this.password = password;
        this.connectionPool = connectionPool;
    }

    @Resource
    public void setDatabaseProperties(DatabaseProperties databaseProperties) {
        this.databaseProperties = databaseProperties;
    }

    public static ConnectionManager create(String url, String user,
                                           String password) {
        List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);

        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            try {
                pool.add(createConnection(url, user, password));
            } catch (SQLException throwables) {
                try {
                    pool.add(createConnection(
                            databaseProperties.getUrlSlave(),
                            databaseProperties.getUserSlave(),
                            databaseProperties.getPasswordSlave()));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return new ConnectionManager(url, user, password, pool);
    }

    public static Connection createConnection(String url, String user, String password) throws SQLException {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperties.getDriverClassName());
        dataSource.setUrl(url);
        dataSource.setUsername(user);
        dataSource.setPassword(password);

        return dataSource.getConnection();
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connectionPool.isEmpty()) {
            if (usedConnections.size() < MAX_POOL_SIZE) {
                connectionPool.add(createConnection(url, user, password));
            } else {
                throw new RuntimeException(
                    "Maximum amount of pooled connections reached, no more available connections!");
            }
        }

        Connection connection = connectionPool
                .remove(connectionPool.size() - 1);

        if (!connection.isValid(MAX_TIMEOUT)) {
            connection = createConnection(url, user, password);
        }

        usedConnections.add(connection);
        return connection;
    }

    @Override
    public boolean releaseConnection(Connection connection) throws SQLException {
        try {
            connectionPool.add(connection);
        } catch (RuntimeException e) {
            connectionPool.add(createConnection(databaseProperties.getUrlSlave(),
                    databaseProperties.getUserSlave(),
                    databaseProperties.getPasswordSlave()));
        }

        return usedConnections.remove(connection);
    }

    @Override
    public int getSize() {
        return connectionPool.size() + usedConnections.size();
    }

    @Override
    public int getUsedConnections() {
        return usedConnections.size();
    }

    @Override
    public String getUrl() {
        return this.url;
    }

    @Override
    public String getUser() {
        return this.user;
    }

    @Override
    public String getPassword() {
        return this.password;
    }
}
