package com.example.ConnectionManager.dbs;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionPool {
    Connection getConnection() throws SQLException;
    boolean releaseConnection(Connection connection) throws SQLException;
    int getSize();
    int getUsedConnections();
    String getUrl();
    String getUser();
    String getPassword();
}
