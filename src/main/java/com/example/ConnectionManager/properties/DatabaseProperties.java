package com.example.ConnectionManager.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class DatabaseProperties {
    @Value("${spring.datasource.master.driverClassName}")
    private String driverClassName;

    @Value("${spring.datasource.master.url}")
    private String urlMaster;
    @Value("${spring.datasource.master.username}")
    private String userMaster;
    @Value("${spring.datasource.master.password}")
    private String passwordMaster;

    @Value("${spring.datasource.slave.url}")
    private String urlSlave;
    @Value("${spring.datasource.slave.username}")
    private String userSlave;
    @Value("${spring.datasource.slave.password}")
    private String passwordSlave;

    public String getDriverClassName() {
        return driverClassName;
    }

    public String getUrlMaster() {
        return urlMaster;
    }

    public String getUserMaster() {
        return userMaster;
    }

    public String getPasswordMaster() {
        return passwordMaster;
    }

    public String getUrlSlave() {
        return urlSlave;
    }

    public String getUserSlave() {
        return userSlave;
    }

    public String getPasswordSlave() {
        return passwordSlave;
    }
}
