package com.example.ConnectionManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConnectionManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConnectionManagerApplication.class, args);
	}

}
